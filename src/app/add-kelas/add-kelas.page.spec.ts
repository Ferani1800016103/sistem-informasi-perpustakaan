import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddKelasPage } from './add-kelas.page';

describe('AddKelasPage', () => {
  let component: AddKelasPage;
  let fixture: ComponentFixture<AddKelasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddKelasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddKelasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
