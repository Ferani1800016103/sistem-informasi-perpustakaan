import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Kelas } from '../models/kelas.model';

@Component({
  selector: 'app-add-kelas',
  templateUrl: './add-kelas.page.html',
  styleUrls: ['./add-kelas.page.scss'],
})
export class AddKelasPage implements OnInit {

  kelas ={} as Kelas;
  selectedFile: any;
  loading: HTMLIonLoadingElement;
  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private afAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private navCtrl: NavController
  ) {}
  
  ngOnInit() {}

  chooseFile (event) {
    this.selectedFile = event.target.files
  }

  


  async createKelas(kelas: Kelas) {
    // console.log(kelas);

    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.collection("kelass").add(kelas);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      loader.dismiss();

      // redirect to kelas page
      this.navCtrl.navigateRoot("tabs/peminjaman");
    }
  }

  formValidation() {
    if (!this.kelas.nkelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.kelas.nguru) {
      // show toast message
      this.showToast("Enter Nama Guru");
      return false;
    }

    if (!this.kelas.npenerbit) {
      // show toast message
      this.showToast("Enter penerbit");
      return false;
    }

    if (!this.kelas.kode) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    if (!this.kelas.tglpinjam) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }





  

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({
      message: 'Please wait...'
    });
    return this.loading.present();
}
}



