import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IPAPageRoutingModule } from './ipa-routing.module';

import { IPAPage } from './ipa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IPAPageRoutingModule
  ],
  declarations: [IPAPage]
})
export class IPAPageModule {}
