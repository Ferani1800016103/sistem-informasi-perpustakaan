import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IPAPage } from './ipa.page';

const routes: Routes = [
  {
    path: '',
    component: IPAPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IPAPageRoutingModule {}
