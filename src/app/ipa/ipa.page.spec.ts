import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IPAPage } from './ipa.page';

describe('IPAPage', () => {
  let component: IPAPage;
  let fixture: ComponentFixture<IPAPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IPAPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IPAPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
