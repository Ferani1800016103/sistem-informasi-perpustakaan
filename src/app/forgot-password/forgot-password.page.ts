import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {

  constructor(
    public fsAuth: AngularFireAuth,
    private toast:ToastController,
    public toastController: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  userAuth:any;
  ionViewDidEnter() {
    this.fsAuth.onAuthStateChanged((user) => {
      if (user) {
        this.userAuth = user;
      }
    })
  }

  async pesanKesalahan() {
    const toast = await this.toastController.create({
      message: 'Your settings have been saved.',
      duration: 2000,
      position:'middle'
    });
    toast.present();
  }

  email:any;
  loading:boolean;
  forgot()
  {
    this.loading=true;
    this.fsAuth.sendPasswordResetEmail(this.email).then(res=>{
      this.loading=false;
      this.presentToast('Tautan untuk pembaruan kata sandi berhasil terkirim melalui email Anda');
      this.router.navigate(['/login']);
    }).catch(error=>{
      this.pesanKesalahan();
    })
  }

  async presentToast(msg) {
    const toast = await this.toast.create({     
      message: msg,
      position: 'middle',
      duration:3000
    });
    toast.present();
  }

}
