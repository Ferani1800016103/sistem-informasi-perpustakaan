import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, Platform, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-peminjaman',
  templateUrl: './peminjaman.page.html',
  styleUrls: ['./peminjaman.page.scss'],
})
export class PeminjamanPage  {
  kelass: any;
  subscription: any;
  kode: any;

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getKelass() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("kelass")
        .snapshotChanges()
        .subscribe(data => {
          this.kelass = data.map(e => {
            return {
              id: e.payload.doc.id,
              nkelas: e.payload.doc.data()["nkelas"],
              nguru: e.payload.doc.data()["nguru"],
              npenerbit: e.payload.doc.data()["npenerbit"],
              kode: e.payload.doc.data()["kode"],
              tglpinjam: e.payload.doc.data()["tglpinjam"],
              tglkembali: e.payload.doc.data()["tglkembali"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  async delete(id: string) {
    // console.log(id);

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    await this.firestore.doc("kelass/" + id).delete();

    // dismiss loader
    loader.dismiss();
  }

  ionViewWillEnter() {
    this.getKelass();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }

}

