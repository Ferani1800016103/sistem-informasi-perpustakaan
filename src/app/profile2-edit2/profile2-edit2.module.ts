import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Profile2Edit2PageRoutingModule } from './profile2-edit2-routing.module';

import { Profile2Edit2Page } from './profile2-edit2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Profile2Edit2PageRoutingModule
  ],
  declarations: [Profile2Edit2Page]
})
export class Profile2Edit2PageModule {}
