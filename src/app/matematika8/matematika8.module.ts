import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Matematika8PageRoutingModule } from './matematika8-routing.module';

import { Matematika8Page } from './matematika8.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Matematika8PageRoutingModule
  ],
  declarations: [Matematika8Page]
})
export class Matematika8PageModule {}
