import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Matematika8Page } from './matematika8.page';

describe('Matematika8Page', () => {
  let component: Matematika8Page;
  let fixture: ComponentFixture<Matematika8Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Matematika8Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Matematika8Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
