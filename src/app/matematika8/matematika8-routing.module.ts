import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Matematika8Page } from './matematika8.page';

const routes: Routes = [
  {
    path: '',
    component: Matematika8Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Matematika8PageRoutingModule {}
