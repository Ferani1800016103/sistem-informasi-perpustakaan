import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IpsPage } from './ips.page';

describe('IpsPage', () => {
  let component: IpsPage;
  let fixture: ComponentFixture<IpsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IpsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IpsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
