import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IpsPage } from './ips.page';

const routes: Routes = [
  {
    path: '',
    component: IpsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IpsPageRoutingModule {}
