import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IpsPageRoutingModule } from './ips-routing.module';

import { IpsPage } from './ips.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IpsPageRoutingModule
  ],
  declarations: [IpsPage]
})
export class IpsPageModule {}
