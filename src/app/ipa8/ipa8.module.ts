import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ipa8PageRoutingModule } from './ipa8-routing.module';

import { Ipa8Page } from './ipa8.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ipa8PageRoutingModule
  ],
  declarations: [Ipa8Page]
})
export class Ipa8PageModule {}
