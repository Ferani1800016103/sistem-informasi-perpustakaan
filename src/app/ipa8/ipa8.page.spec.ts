import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ipa8Page } from './ipa8.page';

describe('Ipa8Page', () => {
  let component: Ipa8Page;
  let fixture: ComponentFixture<Ipa8Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ipa8Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ipa8Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
