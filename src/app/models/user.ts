export interface User 
{
    userId: string;
    userName: string;
    userNim: number;
    userEmail: string;
    userPhone: string;
    userStatus: string;
    userPhoto: string;
    createdAt: number;
    tglpinjam: string;
}
