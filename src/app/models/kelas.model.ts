export interface Kelas {
    nkelas: string;
    nguru: string;
    npenerbit: string;
    status: string;
    kode: string;
    tglpinjam: string;
    tglkembali: string;
    }
