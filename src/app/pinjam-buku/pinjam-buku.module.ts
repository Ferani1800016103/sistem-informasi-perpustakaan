import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PinjamBukuPageRoutingModule } from './pinjam-buku-routing.module';

import { PinjamBukuPage } from './pinjam-buku.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PinjamBukuPageRoutingModule
  ],
  declarations: [PinjamBukuPage]
})
export class PinjamBukuPageModule {}
