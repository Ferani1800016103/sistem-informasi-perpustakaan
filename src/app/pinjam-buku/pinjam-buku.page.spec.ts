import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PinjamBukuPage } from './pinjam-buku.page';

describe('PinjamBukuPage', () => {
  let component: PinjamBukuPage;
  let fixture: ComponentFixture<PinjamBukuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinjamBukuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PinjamBukuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
