import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PinjamBukuPage } from './pinjam-buku.page';

const routes: Routes = [
  {
    path: '',
    component: PinjamBukuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PinjamBukuPageRoutingModule {}
