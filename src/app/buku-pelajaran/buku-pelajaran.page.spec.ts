import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BukuPelajaranPage } from './buku-pelajaran.page';

describe('BukuPelajaranPage', () => {
  let component: BukuPelajaranPage;
  let fixture: ComponentFixture<BukuPelajaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BukuPelajaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BukuPelajaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
