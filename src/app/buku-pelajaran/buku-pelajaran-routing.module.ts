import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BukuPelajaranPage } from './buku-pelajaran.page';

const routes: Routes = [
  {
    path: '',
    component: BukuPelajaranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BukuPelajaranPageRoutingModule {}
