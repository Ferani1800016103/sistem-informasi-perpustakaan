import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BukuPelajaranPageRoutingModule } from './buku-pelajaran-routing.module';

import { BukuPelajaranPage } from './buku-pelajaran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BukuPelajaranPageRoutingModule
  ],
  declarations: [BukuPelajaranPage]
})
export class BukuPelajaranPageModule {}
