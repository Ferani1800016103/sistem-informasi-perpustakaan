import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ips8PageRoutingModule } from './ips8-routing.module';

import { Ips8Page } from './ips8.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ips8PageRoutingModule
  ],
  declarations: [Ips8Page]
})
export class Ips8PageModule {}
