import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ips8Page } from './ips8.page';

const routes: Routes = [
  {
    path: '',
    component: Ips8Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ips8PageRoutingModule {}
