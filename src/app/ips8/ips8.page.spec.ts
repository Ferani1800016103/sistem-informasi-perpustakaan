import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ips8Page } from './ips8.page';

describe('Ips8Page', () => {
  let component: Ips8Page;
  let fixture: ComponentFixture<Ips8Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ips8Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ips8Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
