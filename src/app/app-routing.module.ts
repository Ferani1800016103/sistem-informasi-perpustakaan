import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
{ path: '', redirectTo: '/tabs/urama-guru', pathMatch: 'full'},

{ path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule), canActivate: [AuthGuard]},

{ path: 'utama', loadChildren: () => import('./utama/utama.module').then( m => m.UtamaPageModule), canActivate: [AuthGuard]},
{ path: 'pilihan', loadChildren: () => import('./pilihan/pilihan.module').then( m => m.PilihanPageModule)},


//halaman guru
{ path: '', loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)},
{ path: 'login', loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)},
{ path: 'register', loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)},
//{ path: 'profile', loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule), canActivate: [AuthGuard]},
{ path: 'profile/edit', loadChildren: () => import('./profile-edit/profile-edit.module').then( m => m.ProfileEditPageModule), canActivate: [AuthGuard]},
//{ path: 'utama-guru', loadChildren: () => import('./utama-guru/utama-guru.module').then( m => m.UtamaGuruPageModule)},
//{ path: 'kelas-guru', loadChildren: () => import('./kelas-guru/kelas-guru.module').then( m => m.KelasGuruPageModule)},
{ path: 'add-kelas', loadChildren: () => import('./add-kelas/add-kelas.module').then( m => m.AddKelasPageModule)},
{ path: 'edit-kelas/:id', loadChildren: () => import('./edit-kelas/edit-kelas.module').then( m => m.EditKelasPageModule)},
//{ path: 'setting', loadChildren: () => import('./setting/setting.module').then( m => m.SettingPageModule)},
//{ path: 'chat', loadChildren: () => import('./chat/chat.module').then( m => m.ChatPageModule)},




//halaman siswa
{ path: '', loadChildren: () => import('./tabs2/tabs2.module').then( m => m.Tabs2PageModule)},
{ path: 'login2', loadChildren: () => import('./login2/login2.module').then( m => m.Login2PageModule)},
{ path: 'register2', loadChildren: () => import('./register2/register2.module').then( m => m.Register2PageModule)},
//{ path: 'profile2', loadChildren: () => import('./profile2/profile2.module').then( m => m.Profile2PageModule)},
{ path: 'profile2/edit2', loadChildren: () => import('./profile2-edit2/profile2-edit2.module').then( m => m.Profile2Edit2PageModule)},
//{ path: 'utama-siswa', loadChildren: () => import('./utama-siswa/utama-siswa.module').then( m => m.UtamaSiswaPageModule)},
//{ path: 'kelas-siswa', loadChildren: () => import('./kelas-siswa/kelas-siswa.module').then( m => m.KelasSiswaPageModule)},
//{ path: 'setting2', loadChildren: () => import('./setting2/setting2.module').then( m => m.Setting2PageModule)},
//{ path: 'chat2', loadChildren: () => import('./chat2/chat2.module').then( m => m.Chat2PageModule)},










{ path: 'forgot-password', loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)},
{ path: 'change-password', loadChildren: () => import('./change-password/change-password.module').then( m => m.ChangePasswordPageModule), canActivate: [AuthGuard]},




  {
    path: 'cari',
    loadChildren: () => import('./cari/cari.module').then( m => m.CariPageModule)
  },
  {
    path: 'hasil',
    loadChildren: () => import('./hasil/hasil.module').then( m => m.HasilPageModule)
  },
  { path: 'perpanjang/:id', loadChildren: () => import('./perpanjang/perpanjang.module').then( m => m.PerpanjangPageModule)},
  
  {
    path: 'matematika7peminjaman',
    loadChildren: () => import('./matematika7peminjaman/matematika7peminjaman.module').then( m => m.Matematika7peminjamanPageModule)
  },
 
  {
    path: 'ipa7peminjaman',
    loadChildren: () => import('./ipa7peminjaman/ipa7peminjaman.module').then( m => m.Ipa7peminjamanPageModule)
  },
  {
    path: 'novel',
    loadChildren: () => import('./novel/novel.module').then( m => m.NovelPageModule)
  },
  {
    path: 'carpen',
    loadChildren: () => import('./carpen/carpen.module').then( m => m.CarpenPageModule)
  },
  {
    path: 'pengembalian',
    loadChildren: () => import('./pengembalian/pengembalian.module').then( m => m.PengembalianPageModule)
  },
  {
    path: 'tambah-buku',
    loadChildren: () => import('./tambah-buku/tambah-buku.module').then( m => m.TambahBukuPageModule)
  },
  //{path: 'isi-chat',loadChildren: () => import('./isi-chat/isi-chat.module').then( m => m.IsiChatPageModule)},
  //{ path: 'ips7', loadChildren: () => import('./ips7/ips7.module').then( m => m.Ips7PageModule)},
  //{ path: 'ips8', loadChildren: () => import('./ips8/ips8.module').then( m => m.Ips8PageModule)},
  //{ path: 'ips9', loadChildren: () => import('./ips9/ips9.module').then( m => m.Ips9PageModule)},
  //{ path: 'ipa7', loadChildren: () => import('./ipa7/ipa7.module').then( m => m.Ipa7PageModule)},
  //{ path: 'ipa8', loadChildren: () => import('./ipa8/ipa8.module').then( m => m.Ipa8PageModule)},
  //{ path: 'ipa9', loadChildren: () => import('./ipa9/ipa9.module').then( m => m.Ipa9PageModule)},
  //{ path: 'matematika7', loadChildren: () => import('./matematika7/matematika7.module').then( m => m.Matematika7PageModule)},
  //{ path: 'matematika8', loadChildren: () => import('./matematika8/matematika8.module').then( m => m.Matematika8PageModule)},
  //{ path: 'matematika9', loadChildren: () => import('./matematika9/matematika9.module').then( m => m.Matematika9PageModule)},
  //{ path: 'ips', loadChildren: () => import('./ips/ips.module').then( m => m.IpsPageModule)},
  //{ path: 'ipa', loadChildren: () => import('./ipa/ipa.module').then( m => m.IPAPageModule)},
  //{ path: 'matematika', loadChildren: () => import('./matematika/matematika.module').then( m => m.MatematikaPageModule)},
  //{ path: 'buku-cerita', loadChildren: () => import('./buku-cerita/buku-cerita.module').then( m => m.BukuCeritaPageModule)},
  //{ path: 'buku-pelajaran', loadChildren: () => import('./buku-pelajaran/buku-pelajaran.module').then( m => m.BukuPelajaranPageModule)},
  
  //{ path: 'peminjaman', loadChildren: () => import('./peminjaman/peminjaman.module').then( m => m.PeminjamanPageModule)},
  









//{ path: '**', loadChildren: () => import('./page-not-found/page-not-found.module').then( m => m.PageNotFoundPageModule)},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
