import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ipa7PageRoutingModule } from './ipa7-routing.module';

import { Ipa7Page } from './ipa7.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ipa7PageRoutingModule
  ],
  declarations: [Ipa7Page]
})
export class Ipa7PageModule {}
