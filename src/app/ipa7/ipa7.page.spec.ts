import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ipa7Page } from './ipa7.page';

describe('Ipa7Page', () => {
  let component: Ipa7Page;
  let fixture: ComponentFixture<Ipa7Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ipa7Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ipa7Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
