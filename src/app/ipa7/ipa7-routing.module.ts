import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ipa7Page } from './ipa7.page';

const routes: Routes = [
  {
    path: '',
    component: Ipa7Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ipa7PageRoutingModule {}
