import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { Kelas } from '../models/kelas.model';

@Component({
  selector: 'app-edit-kelas',
  templateUrl: './edit-kelas.page.html',
  styleUrls: ['./edit-kelas.page.scss'],
})
export class EditKelasPage implements OnInit {

  kelas = {} as Kelas;
  id: any;

  constructor(
    private actRoute: ActivatedRoute,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.id = this.actRoute.snapshot.paramMap.get("id");
  }
 
 
  ngOnInit() {
    this.getKelasById(this.id);
  }

  async getKelasById(id: string) {
    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Please wait..."
    });
    loader.present();

    this.firestore
      .doc("kelass/" + id)
      .valueChanges()
      .subscribe(data => {
        this.kelas.nkelas = data["nkelas"];
        this.kelas.nguru = data["nguru"];
        this.kelas.kode = data["kode"];

        // dismiss loader
        loader.dismiss();
      });
  }

  async updateKelas(kelas: Kelas) {
    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.doc("kelass/" + this.id).update(kelas);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      await loader.dismiss();

      // redirect to home page
      this.navCtrl.navigateRoot("tabs/kelas-guru");
    }
  }

  formValidation() {
    if (!this.kelas.nkelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.kelas.nguru) {
      // show toast message
      this.showToast("Enter Nama Guru");
      return false;
    }

    if (!this.kelas.kode) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }


}
