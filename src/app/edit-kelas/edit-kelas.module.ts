import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditKelasPageRoutingModule } from './edit-kelas-routing.module';

import { EditKelasPage } from './edit-kelas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditKelasPageRoutingModule
  ],
  declarations: [EditKelasPage]
})
export class EditKelasPageModule {}
