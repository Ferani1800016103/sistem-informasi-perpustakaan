import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TabsPage } from './tabs.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
    {
      path: 'utama-guru',
      children: [
        {
          path: '',
          loadChildren: '../utama-guru/utama-guru.module#UtamaGuruPageModule'
        }
      ]
    },
    {
      path: 'chat',
      children: [
        {
          path: '',
          loadChildren: '../chat/chat.module#ChatPageModule'
        }
      ]
    },
    {
      path: 'profile',
      children: [
        {
          path: '',
          loadChildren: '../profile/profile.module#ProfilePageModule'
        }
      ]
    },
    {
      path: 'setting',
      children: [
        {
          path: '',
          loadChildren: '../setting/setting.module#SettingPageModule'
        }
      ]
    },
    {
      path: 'laporan',
      children: [
        {
          path: '',
          loadChildren: '../laporan/laporan.module#LaporanPageModule'
        }
      ]
    },
    {
      path: 'kelas-guru',
      children: [
        {
          path: '',
          loadChildren: '../kelas-guru/kelas-guru.module#KelasGuruPageModule'
        }
      ]
    },
    {
      path: 'peminjaman',
      children: [
        {
          path: '',
          loadChildren: '../peminjaman/peminjaman.module#PeminjamanPageModule'
        }
      ]
    },
    {
      path: 'perpanjang',
      children: [
        {
          path: '',
          loadChildren: '../perpanjang/perpanjang.module#PerpanjangPageModule'
        }
      ]
    },
    
    {
      path: 'buku-pelajaran',
      children: [
        {
          path: '',
          loadChildren: '../buku-pelajaran/buku-pelajaran.module#BukuPelajaranPageModule'
        }
      ]
    },
    {
      path: 'matematika',
      children: [
        {
          path: '',
          loadChildren: '../matematika/matematika.module#MatematikaPageModule'
        }
      ]
    },
    {
      path: 'matematika7',
      children: [
        {
          path: '',
          loadChildren: '../matematika7/matematika7.module#Matematika7PageModule'
        }
      ]
    },
    {
      path: 'matematika8',
      children: [
        {
          path: '',
          loadChildren: '../matematika8/matematika8.module#Matematika8PageModule'
        }
      ]
    },
    {
      path: 'matematika9',
      children: [
        {
          path: '',
          loadChildren: '../matematika9/matematika9.module#Matematika9PageModule'
        }
      ]
    },
    {
      path: 'ipa',
      children: [
        {
          path: '',
          loadChildren: '../ipa/ipa.module#IPAPageModule'
        }
      ]
    },
    {
      path: 'ipa7',
      children: [
        {
          path: '',
          loadChildren: '../ipa7/ipa7.module#Ipa7PageModule'
        }
      ]
    },
    {
      path: 'ipa8',
      children: [
        {
          path: '',
          loadChildren: '../ipa8/ipa8.module#Ipa8PageModule'
        }
      ]
    },
    {
      path: 'ipa9',
      children: [
        {
          path: '',
          loadChildren: '../ipa9/ipa9.module#Ipa9PageModule'
        }
      ]
    },
    {
      path: 'ips',
      children: [
        {
          path: '',
          loadChildren: '../ips/ips.module#IpsPageModule'
        }
      ]
    },
    {
      path: 'ips7',
      children: [
        {
          path: '',
          loadChildren: '../ips7/ips7.module#Ips7PageModule'
        }
      ]
    },
    {
      path: 'ips8',
      children: [
        {
          path: '',
          loadChildren: '../ips8/ips8.module#Ips8PageModule'
        }
      ]
    },
    {
      path: 'ips9',
      children: [
        {
          path: '',
          loadChildren: '../ips9/ips9.module#Ips9PageModule'
        }
      ]
    },
    {
      path: 'buku-cerita',
      children: [
        {
          path: '',
          loadChildren: '../buku-cerita/buku-cerita.module#BukuCeritaPageModule'
        }
      ]
    },
     {
      path: 'buku-cerita',
      children: [
        {
          path: '',
          loadChildren: '../buku-cerita/buku-cerita.module#BukuCeritaPageModule'
        }
      ]
    },
    {
      path: 'isi-chat',
      children: [
        {
          path: '',
          loadChildren: '../isi-chat/isi-chat.module#IsiChatPageModule'
        }
      ]
    },
  ]
  },
  {
    path: '',
    redirectTo: '/tabs/utama-guru',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)

  ],
  declarations: [TabsPage],
  exports: [RouterModule]
})
export class TabsPageModule {}
