import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BukuCeritaPage } from './buku-cerita.page';

const routes: Routes = [
  {
    path: '',
    component: BukuCeritaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BukuCeritaPageRoutingModule {}
