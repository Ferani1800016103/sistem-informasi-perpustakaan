import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BukuCeritaPageRoutingModule } from './buku-cerita-routing.module';

import { BukuCeritaPage } from './buku-cerita.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BukuCeritaPageRoutingModule
  ],
  declarations: [BukuCeritaPage]
})
export class BukuCeritaPageModule {}
