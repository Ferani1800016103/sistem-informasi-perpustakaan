import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BukuCeritaPage } from './buku-cerita.page';

describe('BukuCeritaPage', () => {
  let component: BukuCeritaPage;
  let fixture: ComponentFixture<BukuCeritaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BukuCeritaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BukuCeritaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
