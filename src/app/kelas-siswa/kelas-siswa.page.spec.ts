import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KelasSiswaPage } from './kelas-siswa.page';

describe('KelasSiswaPage', () => {
  let component: KelasSiswaPage;
  let fixture: ComponentFixture<KelasSiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KelasSiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KelasSiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
