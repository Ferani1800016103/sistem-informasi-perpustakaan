import { Component, OnInit } from '@angular/core';
import { ToastController, LoadingController, Platform } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-kelas-siswa',
  templateUrl: './kelas-siswa.page.html',
  styleUrls: ['./kelas-siswa.page.scss'],
})
export class KelasSiswaPage{

  kelass: any;
  subscription: any;
  kode: any;

  constructor(
    private toastCtrl: ToastController,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private platform: Platform
  ) {}

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribe(() => {
      navigator["app"].exitApp();
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  async getKelass() {
    // console.log("get kelass");

    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Silahkan Tunggu..."
    });
    loader.present();

    try {
      this.firestore
        .collection("kelass")
        .snapshotChanges()
        .subscribe(data => {
          this.kelass = data.map(e => {
            return {
              id: e.payload.doc.id,
              nkelas: e.payload.doc.data()["nkelas"],
              nguru: e.payload.doc.data()["nguru"],
              kode: e.payload.doc.data()["kode"]
            };
          });

          // dismiss loader
          loader.dismiss();
        });
    } catch (e) {
      this.showToast(e);
    }
  }

  
  ionViewWillEnter() {
    this.getKelass();
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }


}
