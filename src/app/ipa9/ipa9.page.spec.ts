import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ipa9Page } from './ipa9.page';

describe('Ipa9Page', () => {
  let component: Ipa9Page;
  let fixture: ComponentFixture<Ipa9Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ipa9Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ipa9Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
