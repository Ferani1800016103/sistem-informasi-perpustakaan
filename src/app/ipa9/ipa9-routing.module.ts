import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ipa9Page } from './ipa9.page';

const routes: Routes = [
  {
    path: '',
    component: Ipa9Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ipa9PageRoutingModule {}
