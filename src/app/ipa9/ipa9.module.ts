import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ipa9PageRoutingModule } from './ipa9-routing.module';

import { Ipa9Page } from './ipa9.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ipa9PageRoutingModule
  ],
  declarations: [Ipa9Page]
})
export class Ipa9PageModule {}
