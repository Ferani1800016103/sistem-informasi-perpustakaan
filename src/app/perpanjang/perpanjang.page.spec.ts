import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerpanjangPage } from './perpanjang.page';

describe('PerpanjangPage', () => {
  let component: PerpanjangPage;
  let fixture: ComponentFixture<PerpanjangPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerpanjangPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PerpanjangPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
