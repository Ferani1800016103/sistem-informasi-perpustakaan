import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerpanjangPageRoutingModule } from './perpanjang-routing.module';

import { PerpanjangPage } from './perpanjang.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerpanjangPageRoutingModule
  ],
  declarations: [PerpanjangPage]
})
export class PerpanjangPageModule {}
