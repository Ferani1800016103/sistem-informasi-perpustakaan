import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { Kelas } from '../models/kelas.model';

@Component({
  selector: 'app-perpanjang',
  templateUrl: './perpanjang.page.html',
  styleUrls: ['./perpanjang.page.scss'],
})
export class PerpanjangPage implements OnInit {

  kelas = {} as Kelas;
  id: any;

  constructor(
    private actRoute: ActivatedRoute,
    private firestore: AngularFirestore,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.id = this.actRoute.snapshot.paramMap.get("id");
  }
 
 
  ngOnInit() {
    this.getKelasById(this.id);
  }

  async getKelasById(id: string) {
    // show loader
    let loader = await this.loadingCtrl.create({
      message: "Please wait..."
    });
    loader.present();

    this.firestore
      .doc("kelass/" + id)
      .valueChanges()
      .subscribe(data => {
        this.kelas.nkelas = data["nkelas"];
        this.kelas.nguru = data["nguru"];
        this.kelas.npenerbit = data["npenerbit"];
        this.kelas.kode = data["kode"];
        this.kelas.tglpinjam = data["tglpinjam"];
        this.kelas.tglkembali = data["tglkembali"];

        // dismiss loader
        loader.dismiss();
      });
  }

  async updateKelas(kelas: Kelas) {
    if (this.formValidation()) {
      // console.log("ready to submit");

      // show loader
      let loader = await this.loadingCtrl.create({
        message: "Silahkan Tunggu..."
      });
      loader.present();

      try {
        await this.firestore.doc("kelass/" + this.id).update(kelas);
      } catch (e) {
        this.showToast(e);
      }

      // dismiss loader
      await loader.dismiss();

      // redirect to home page
      this.navCtrl.navigateRoot("tabs/peminjaman");
    }
  }

  formValidation() {
    if (!this.kelas.nkelas) {
      // show toast message
      this.showToast("Enter Nama Kelas");
      return false;
    }

    if (!this.kelas.nguru) {
      // show toast message
      this.showToast("Enter Nama Guru");
      return false;
    }

    if (!this.kelas.npenerbit) {
      // show toast message
      this.showToast("Enter Nama Guru");
      return false;
    }

    if (!this.kelas.kode) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    if (!this.kelas.tglpinjam) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    if (!this.kelas.tglkembali) {
      // show toast message
      this.showToast("Enter kode");
      return false;
    }

    return true;
  }

  showToast(message: string) {
    this.toastCtrl
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }
}