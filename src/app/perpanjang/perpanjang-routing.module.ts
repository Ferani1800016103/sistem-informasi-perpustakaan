import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerpanjangPage } from './perpanjang.page';

const routes: Routes = [
  {
    path: '',
    component: PerpanjangPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerpanjangPageRoutingModule {}
