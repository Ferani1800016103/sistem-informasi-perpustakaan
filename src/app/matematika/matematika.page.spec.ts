import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MatematikaPage } from './matematika.page';

describe('MatematikaPage', () => {
  let component: MatematikaPage;
  let fixture: ComponentFixture<MatematikaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatematikaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MatematikaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
