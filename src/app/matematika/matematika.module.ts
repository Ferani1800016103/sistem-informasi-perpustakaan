import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MatematikaPageRoutingModule } from './matematika-routing.module';

import { MatematikaPage } from './matematika.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatematikaPageRoutingModule
  ],
  declarations: [MatematikaPage]
})
export class MatematikaPageModule {}
