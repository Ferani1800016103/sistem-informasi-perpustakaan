import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MatematikaPage } from './matematika.page';

const routes: Routes = [
  {
    path: '',
    component: MatematikaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MatematikaPageRoutingModule {}
