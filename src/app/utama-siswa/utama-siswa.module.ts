import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UtamaSiswaPageRoutingModule } from './utama-siswa-routing.module';
import { UtamaSiswaPage } from './utama-siswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UtamaSiswaPageRoutingModule
  ],
  declarations: [UtamaSiswaPage]
})
export class UtamaSiswaPageModule {}
