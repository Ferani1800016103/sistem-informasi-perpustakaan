import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UtamaSiswaPage } from './utama-siswa.page';

describe('UtamaSiswaPage', () => {
  let component: UtamaSiswaPage;
  let fixture: ComponentFixture<UtamaSiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtamaSiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UtamaSiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
