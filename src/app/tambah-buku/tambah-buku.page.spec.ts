import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TambahBukuPage } from './tambah-buku.page';

describe('TambahBukuPage', () => {
  let component: TambahBukuPage;
  let fixture: ComponentFixture<TambahBukuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahBukuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TambahBukuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
