import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UtamaGuruPage } from './utama-guru.page';

describe('UtamaGuruPage', () => {
  let component: UtamaGuruPage;
  let fixture: ComponentFixture<UtamaGuruPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtamaGuruPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UtamaGuruPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
