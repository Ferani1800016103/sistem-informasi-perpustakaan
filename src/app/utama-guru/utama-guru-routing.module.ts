import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UtamaGuruPage } from './utama-guru.page';



const routes: Routes = [
  {
    path: '',
    component: UtamaGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UtamaGuruPageRoutingModule {}
