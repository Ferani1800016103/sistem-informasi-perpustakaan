import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Matematika7peminjamanPage } from './matematika7peminjaman.page';

const routes: Routes = [
  {
    path: '',
    component: Matematika7peminjamanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Matematika7peminjamanPageRoutingModule {}
