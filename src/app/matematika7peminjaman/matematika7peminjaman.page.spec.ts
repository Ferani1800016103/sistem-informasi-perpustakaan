import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Matematika7peminjamanPage } from './matematika7peminjaman.page';

describe('Matematika7peminjamanPage', () => {
  let component: Matematika7peminjamanPage;
  let fixture: ComponentFixture<Matematika7peminjamanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Matematika7peminjamanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Matematika7peminjamanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
