import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Matematika7peminjamanPageRoutingModule } from './matematika7peminjaman-routing.module';

import { Matematika7peminjamanPage } from './matematika7peminjaman.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Matematika7peminjamanPageRoutingModule
  ],
  declarations: [Matematika7peminjamanPage]
})
export class Matematika7peminjamanPageModule {}
