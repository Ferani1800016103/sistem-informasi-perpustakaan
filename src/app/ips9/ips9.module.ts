import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ips9PageRoutingModule } from './ips9-routing.module';

import { Ips9Page } from './ips9.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ips9PageRoutingModule
  ],
  declarations: [Ips9Page]
})
export class Ips9PageModule {}
