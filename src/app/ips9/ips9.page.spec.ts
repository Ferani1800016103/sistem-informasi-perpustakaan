import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ips9Page } from './ips9.page';

describe('Ips9Page', () => {
  let component: Ips9Page;
  let fixture: ComponentFixture<Ips9Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ips9Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ips9Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
