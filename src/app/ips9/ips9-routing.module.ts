import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ips9Page } from './ips9.page';

const routes: Routes = [
  {
    path: '',
    component: Ips9Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ips9PageRoutingModule {}
