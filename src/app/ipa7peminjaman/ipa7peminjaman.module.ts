import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ipa7peminjamanPageRoutingModule } from './ipa7peminjaman-routing.module';

import { Ipa7peminjamanPage } from './ipa7peminjaman.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ipa7peminjamanPageRoutingModule
  ],
  declarations: [Ipa7peminjamanPage]
})
export class Ipa7peminjamanPageModule {}
