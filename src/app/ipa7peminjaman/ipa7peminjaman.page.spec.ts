import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ipa7peminjamanPage } from './ipa7peminjaman.page';

describe('Ipa7peminjamanPage', () => {
  let component: Ipa7peminjamanPage;
  let fixture: ComponentFixture<Ipa7peminjamanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ipa7peminjamanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ipa7peminjamanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
