import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ipa7peminjamanPage } from './ipa7peminjaman.page';

const routes: Routes = [
  {
    path: '',
    component: Ipa7peminjamanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ipa7peminjamanPageRoutingModule {}
