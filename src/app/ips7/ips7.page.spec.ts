import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Ips7Page } from './ips7.page';

describe('Ips7Page', () => {
  let component: Ips7Page;
  let fixture: ComponentFixture<Ips7Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ips7Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Ips7Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
