import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Ips7Page } from './ips7.page';

const routes: Routes = [
  {
    path: '',
    component: Ips7Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Ips7PageRoutingModule {}
