import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Ips7PageRoutingModule } from './ips7-routing.module';

import { Ips7Page } from './ips7.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ips7PageRoutingModule
  ],
  declarations: [Ips7Page]
})
export class Ips7PageModule {}
