import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarpenPage } from './carpen.page';

const routes: Routes = [
  {
    path: '',
    component: CarpenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarpenPageRoutingModule {}
