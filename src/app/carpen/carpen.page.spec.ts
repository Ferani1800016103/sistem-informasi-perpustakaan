import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CarpenPage } from './carpen.page';

describe('CarpenPage', () => {
  let component: CarpenPage;
  let fixture: ComponentFixture<CarpenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarpenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CarpenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
