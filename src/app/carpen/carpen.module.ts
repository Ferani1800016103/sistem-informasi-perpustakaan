import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarpenPageRoutingModule } from './carpen-routing.module';

import { CarpenPage } from './carpen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarpenPageRoutingModule
  ],
  declarations: [CarpenPage]
})
export class CarpenPageModule {}
