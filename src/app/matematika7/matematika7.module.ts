import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Matematika7PageRoutingModule } from './matematika7-routing.module';

import { Matematika7Page } from './matematika7.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Matematika7PageRoutingModule
  ],
  declarations: [Matematika7Page]
})
export class Matematika7PageModule {}
