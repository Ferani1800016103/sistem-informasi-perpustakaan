import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Matematika7Page } from './matematika7.page';

const routes: Routes = [
  {
    path: '',
    component: Matematika7Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Matematika7PageRoutingModule {}
