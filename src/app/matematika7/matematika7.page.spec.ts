import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Matematika7Page } from './matematika7.page';

describe('Matematika7Page', () => {
  let component: Matematika7Page;
  let fixture: ComponentFixture<Matematika7Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Matematika7Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Matematika7Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
