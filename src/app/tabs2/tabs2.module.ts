import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Tabs2PageRoutingModule } from './tabs2-routing.module';

import { Tabs2Page } from './tabs2.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'tabs2',
    component: Tabs2Page,
    children: [
    {
      path: 'utama-siswa',
      children: [
        {
          path: '',
          loadChildren: '../utama-siswa/utama-siswa.module#UtamaSiswaPageModule'
        }
      ]
    },
    {
      path: 'chat',
      children: [
        {
          path: '',
          loadChildren: '../chat/chat.module#ChatPageModule'
        }
      ]
    },
    {
      path: 'profile',
      children: [
        {
          path: '',
          loadChildren: '../profile/profile.module#ProfilePageModule'
        }
      ]
    },
    {
      path: 'setting',
      children: [
        {
          path: '',
          loadChildren: '../setting/setting.module#SettingPageModule'
        }
      ]
    },
    {
      path: 'laporan',
      children: [
        {
          path: '',
          loadChildren: '../laporan/laporan.module#LaporanPageModule'
        }
      ]
    },
    {
      path: 'kelas-guru',
      children: [
        {
          path: '',
          loadChildren: '../kelas-guru/kelas-guru.module#KelasGuruPageModule'
        }
      ]
    },
  ]
  },
  {
    path: '',
    redirectTo: 'tabs2/utama-siswa',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Tabs2PageRoutingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Tabs2Page],
  exports: [RouterModule]
})
export class Tabs2PageModule {}
