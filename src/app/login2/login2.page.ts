import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login2',
  templateUrl: './login2.page.html',
  styleUrls: ['./login2.page.scss'],
})
export class Login2Page implements OnInit 
{
  email: string;
  password: string;

  constructor
  (
    private auth: AuthService,
    private toastr: ToastController
  ) { }

  ngOnInit() {
  }

  login2()
  {
    if(this.email && this.password)
    {
      this.auth.signIn2(this.email, this.password);
    }
  }

  async toast(message, status)
  {
    const toast = await this.toastr.create({
      message: message,
      color: status,
      position: 'top',
      duration: 2000
    });

    toast.present();
  } // end of toast

}
