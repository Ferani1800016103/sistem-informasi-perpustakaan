import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IsiChatPageRoutingModule } from './isi-chat-routing.module';

import { IsiChatPage } from './isi-chat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IsiChatPageRoutingModule
  ],
  declarations: [IsiChatPage]
})
export class IsiChatPageModule {}
