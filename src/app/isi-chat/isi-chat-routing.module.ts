import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsiChatPage } from './isi-chat.page';

const routes: Routes = [
  {
    path: '',
    component: IsiChatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IsiChatPageRoutingModule {}
