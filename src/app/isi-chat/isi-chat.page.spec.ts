import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IsiChatPage } from './isi-chat.page';

describe('IsiChatPage', () => {
  let component: IsiChatPage;
  let fixture: ComponentFixture<IsiChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsiChatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IsiChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
