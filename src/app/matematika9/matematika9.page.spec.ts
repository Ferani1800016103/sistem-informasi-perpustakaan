import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Matematika9Page } from './matematika9.page';

describe('Matematika9Page', () => {
  let component: Matematika9Page;
  let fixture: ComponentFixture<Matematika9Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Matematika9Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Matematika9Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
