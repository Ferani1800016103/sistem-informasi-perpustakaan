import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Matematika9PageRoutingModule } from './matematika9-routing.module';

import { Matematika9Page } from './matematika9.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Matematika9PageRoutingModule
  ],
  declarations: [Matematika9Page]
})
export class Matematika9PageModule {}
