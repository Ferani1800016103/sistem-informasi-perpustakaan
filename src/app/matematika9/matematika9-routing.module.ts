import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Matematika9Page } from './matematika9.page';

const routes: Routes = [
  {
    path: '',
    component: Matematika9Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Matematika9PageRoutingModule {}
