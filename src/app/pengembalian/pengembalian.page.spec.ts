import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PengembalianPage } from './pengembalian.page';

describe('PengembalianPage', () => {
  let component: PengembalianPage;
  let fixture: ComponentFixture<PengembalianPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengembalianPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PengembalianPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
