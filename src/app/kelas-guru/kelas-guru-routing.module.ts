import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KelasGuruPage } from './kelas-guru.page';

const routes: Routes = [
  {
    path: '',
    component: KelasGuruPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KelasGuruPageRoutingModule {}
