import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KelasGuruPageRoutingModule } from './kelas-guru-routing.module';

import { KelasGuruPage } from './kelas-guru.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KelasGuruPageRoutingModule
  ],
  declarations: [KelasGuruPage]
})
export class KelasGuruPageModule {}
